﻿    namespace Common
{
    public enum MenuOperation
    {
        AddNote = 1,
        EditNote = 2,
        DeleteNote = 3,
        StopProgram = 4,
    }
}
