﻿using Common;
using Logic;

namespace UI
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            IDataManager dataManager = new DataManager();
            IMenuProvider menuProvider = new MenuProvider();
            IConsoleManager consoleManager = new ConsoleManager();

            dataManager.LoadNotes();

            while (!MenuProvider.IsProgramStoped)
            {
                consoleManager.PrintAllNotes();

                consoleManager.DisplayMenu();

                menuProvider.RunMenu();
            }
        }
    }
}
