﻿namespace UI
{
    public interface IConsoleManager
    {
        public void DisplayText(string text);

        public void DisplayMenu();

        public string ReadData();

        public void ClearConsole();

        public void PrintAllNotes();
    }
}
