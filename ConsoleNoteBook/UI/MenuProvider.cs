﻿using System;
using System.Collections.Generic;
using System.Text;
using Logic;
using Common;

namespace UI
{
    public class MenuProvider : IMenuProvider
    {
        public static bool IsProgramStoped { get; set; } = false;

        public void AddNote()
        {
            INoteManager noteManager = new NoteManager(); 
            IConsoleManager manager = new ConsoleManager();

            manager.DisplayText(StringConstants.PleaseEnterHeadline);
            string headline = manager.ReadData();

            manager.DisplayText(StringConstants.PleaseEnterContent);
            string content = manager.ReadData();

            noteManager.AddNote(headline, content);
        }

        public void EditNote()
        {
            IConsoleManager manager = new ConsoleManager();
            INoteManager noteManager = new NoteManager();

            manager.DisplayText(StringConstants.PleaseEnterId);
            int id;

            while (!int.TryParse(manager.ReadData(), out id))
            {
                manager.DisplayText(StringConstants.PleaseCheckYourInput);
            }

            if (Note.IsIdExist(id))
            {
                manager.DisplayText(StringConstants.PleaseEnterNewHeadline);
                string newHeadline = manager.ReadData();
                manager.DisplayText(StringConstants.PleaseEnterNewContent);
                string newContent = manager.ReadData();
                noteManager.EditNote(id, newHeadline, newContent);
            }
            else
            {
                manager.DisplayText(StringConstants.WrongInputId);
            }
        }

        public void DeleteNote()
        {
            INoteManager noteManager = new NoteManager();
            IConsoleManager manager = new ConsoleManager();

            int id;

            manager.DisplayText(StringConstants.PleaseEnterId);

            while (!int.TryParse(manager.ReadData(), out id))
            {
                manager.DisplayText(StringConstants.PleaseCheckYourInput);
            }

            noteManager.DeleteNote(id);
        }

        public void StopProgramm()
        {
            IDataManager dataManager = new DataManager();
            dataManager.SaveNotes(NoteManager.Notes);
            IsProgramStoped = true;
        }

        public void RunMenu()
        {
            IConsoleManager manager = new ConsoleManager();

            MenuOperation userChoice = (MenuOperation)int.Parse(manager.ReadData());

            switch (userChoice)
            {
                case MenuOperation.AddNote:
                    AddNote();
                    break;

                case MenuOperation.EditNote:
                    EditNote();
                    break;

                case MenuOperation.DeleteNote:
                    DeleteNote();
                    break;

                case MenuOperation.StopProgram:
                    StopProgramm();
                    break;

                default:
                    manager.DisplayText(StringConstants.PleaseCheckYourInput);
                    break;
            }
        }
    }
}
