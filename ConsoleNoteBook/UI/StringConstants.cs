﻿namespace UI
{
    public static class StringConstants
    {
        public const string PleaseCheckYourInput = "Пожалуйста проверьте правильность введенных вами значений";

        public const string PleaseEnterId = "Пожалуйста введите id";

        public const string FirstMenuLine = "1) добавить запись";

        public const string SecondMenuLine = "2) изменить запись";

        public const string ThirdMenuLine = "3) удалить запись";

        public const string FourthMenuLine = "4) завершить сеанс работы в программе";

        public const string PleaseEnterHeadline = "Введите заголовок записки";

        public const string PleaseEnterContent = "Введите содержание записки";

        public const string PleaseEnterNewHeadline = "Введите новый заголовок";

        public const string PleaseEnterNewContent = "Введите новое описание";

        public const string PleaseRepeatOperation = "Вы ввели что то не то попробуйте ещё раз.";

        public const string WrongInputId = "Вы ввели неверный id";
    }
}
