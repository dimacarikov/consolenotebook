﻿using System;
using Logic;
using Common;

namespace UI
{
    public class ConsoleManager : IConsoleManager
    {
        public void DisplayText(string text)
        {
            Console.WriteLine(text);
        }

        public void DisplayMenu()
        {
            DisplayText(StringConstants.FirstMenuLine);
            DisplayText(StringConstants.SecondMenuLine);
            DisplayText(StringConstants.ThirdMenuLine);
            DisplayText(StringConstants.FourthMenuLine);
        }

        public void PrintAllNotes()
        {
            IConsoleManager manager = new ConsoleManager();

            foreach (var note in NoteManager.Notes)
            {
                manager.DisplayText($"\t Записка номер: {Note.GetNoteId(note)}, " +
                    $"Заголовок: {note.Headline}," +
                    $" Описание: {note.Content}," +
                    $" Дата добавления: {note.Date} ");
            }
        }

        public string ReadData()
        {
            return Console.ReadLine();
        }

        public void ClearConsole()
        {
            Console.Clear();
        }
    }
}
