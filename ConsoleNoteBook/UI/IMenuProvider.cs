﻿namespace UI
{
    public interface IMenuProvider
    { 
        public void AddNote();

        public void EditNote();

        public void DeleteNote();

        public void StopProgramm();

        public void RunMenu();
    }
}
