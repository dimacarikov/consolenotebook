﻿using System;
using System.Collections.Generic;

namespace Logic
{
    public class NoteManager : INoteManager
    {
        public static List<Note> Notes { get; private set; } = new List<Note>();

        public void AddNote(string headline, string content)
        {
            Notes.Add(new Note(headline, content, DateTime.Now));
        }

        public void EditNote(int id, string newHeadline, string newContent)
        {
            if (Note.IsIdExist(id))
            {
                Notes[id].Date = DateTime.Now;
                Notes[id].Headline = newHeadline;
                Notes[id].Content = newContent;
            }
        }

        public void DeleteNote(int id)
        {
            if (Note.IsIdExist(id))
            {
                Notes.RemoveAt(id);
            }
        }
    }
}
