﻿using System;

namespace Logic
{
    public class Note
    {
        public string Headline { get; set; }

        public string Content { get; set; }

        public DateTime  Date { get; set; }

        private static int idCounter = NoteManager.Notes.Count;

        public int Id { get; set; }

        public Note(string headline, string content, DateTime date)
        {
            Id = idCounter++;
            Content = content;
            Headline = headline;
            Date = date;
        }

        public Note()
        {

        }

        public static bool IsIdExist(int id)
        {
            foreach (var note in NoteManager.Notes)
            {
                if (id == note.Id)
                {
                    return true;
                }
            }

            return false;
        }

        public static int GetNoteId(Note note)
        {
            return note.Id;
        }
    }
}
