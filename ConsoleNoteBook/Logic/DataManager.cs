﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.IO;

namespace Logic
{
    public class DataManager : IDataManager
    {
        private const string jsonFile = "output.json";

        public void SaveNotes(List<Note> notes)
        {
            if (!File.Exists(jsonFile))
            {
                File.Create(jsonFile);
            }

            using StreamWriter sw = new StreamWriter(jsonFile);

            for (int i = 0; i < notes.Count; i++)
            {
                sw.WriteLine(JsonSerializer.Serialize(notes[i]));
            }
        }

        public void LoadNotes()
        {
            if (File.Exists(jsonFile))
            {
                using StreamReader sr = new StreamReader(jsonFile);

                while (sr.Peek() != -1)
                {
                    string json = sr.ReadLine().ToString();

                    Note note = JsonSerializer.Deserialize<Note>(json);

                    NoteManager.Notes.Add(note);
                }
            }
        }
    }
}
