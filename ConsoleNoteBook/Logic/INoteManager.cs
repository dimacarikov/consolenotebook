﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public interface INoteManager
    {
        public static List<Note> Notes { get; private set; }

        public void AddNote(string headline, string content);

        public void EditNote(int id, string newHeadline, string newContent);

        public void DeleteNote(int id);
    }
}
