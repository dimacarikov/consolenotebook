﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public interface IDataManager
    {
        public void SaveNotes(List<Note> notes);

        public void LoadNotes();

    }
}
